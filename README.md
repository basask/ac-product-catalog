# Things not delivered

## In memory database

The backend framework used in this project is Django and Django-Rest-Framework (Rest API).

Django supports, out of the box, connections to in memory databases. But its does not supports database migrations for these type of databases in other context other than tests.

As a result it not suport in memory databases in production.

Some efforts could be done to leverage the behavior of tests contexts to the production context. However that was not done due to time limitations.

# Notes

## Unexplored API capabilities
Although the API is fully functional and tested against its requisites some of its functionalities couldn't be explored in the client application.

The functionality not explored by the client was the product hierarchy mechanism. For demonstration of this behavior please refer to backend tests.

The reason of that is also lack of time to explore this possibilities.

## Automated tests

At this point only the Backend API was covered with UNIT tests.
Even thou the backend has almost 95% of its code covered I acknowledge some features should be tested more extensively.

# How to compile and run the application.

## Compiling Frontend

```bash
cd frontend

# Install node dependencies. It could take some time. I suggest you to grab some coffee =)
npm install

# build webpack bundle
npm run build
```

## Backend

If you have make in our siste follow this instructions:
``` bash
cd backend
make setup
make run
```

You can also do all the steps by hand. The steps are:
``` bash
cd backend

# Install python requirements
pip install -r requirements.txt

# Make django migrations
python manage.py makemigrations catalog

# Apply django migrations
python manage.py migrate

# run backend on port 8000
python manage.py runserver 0.0.0.0:8000
```

# How to run the suite of automated tests.

With make/Makefile
```bash
cd backend
make tests
```

Without make/Makefile
```bash
cd backend

# Run tests with coverage in catalog application
coverage run --source='./catalog' manage.py test

# (Optional) Annotate source code to review blind spots in tests
coverage annotate -d ./coverage

# Show coverage report
coverage report
```

# API

| Route | Description |
| ----- | ----------- |
| /catalog/api/images/upload/<product_uid> | upload images|
| /catalog/api/products/ | List all products |
| /catalog/api/products/<product_uid>/ | Detail product |
| /catalog/api/products/<product_uid>/children/ | List products children |
| /catalog/api/products/<product_uid>/images/ | List products Images |
| /catalog/api/products/<product_uid>/parent/ | Get product parent |
