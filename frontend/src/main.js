// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import config from './config'

import VueResource from 'vue-resource'
import VueMaterial from 'vue-material'
import VueCarousel from 'vue-carousel'
import ProductCard from './components/ProductCard.vue'

import Bootstrap from 'bootstrap/dist/css/bootstrap.css'

import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss'
import 'vue-material/dist/vue-material.css'

Vue.component('product-card', ProductCard)

Vue.use(VueCarousel)
Vue.use(VueResource)
Vue.use(Bootstrap)
Vue.use(VueMaterial)

Vue.material.registerTheme('default', {
  primary: 'blue',
  accent: 'red',
  warn: 'red',
  background: 'white'
})

Vue.config.productionTip = false
// Vue.http.options.root = 'http://localhost:8000/catalog/api/'
Vue.http.options.root = config.API

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
