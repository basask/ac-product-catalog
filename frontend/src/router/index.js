import Vue from 'vue'
import Router from 'vue-router'
// import Home from '@/components/Home'
import ProductList from '@/components/ProductList'
import ProductDetail from '@/components/ProductDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'product-list',
      component: ProductList
    },
    {
      path: '/product/:uid',
      name: 'product-detail',
      component: ProductDetail
    },
    {
      path: '/new-product/',
      name: 'product-new',
      component: ProductDetail
    }
  ]
})
