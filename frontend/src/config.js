const config = {
  API: 'http://localhost:8000/catalog/api/',
  STATIC: 'http://localhost:8000/static/'
}

export default config
