import * as axios from 'axios'

const BASE_URL = 'http://localhost:8000'

function upload (formData, ownerUid) {
  let url = `${BASE_URL}/catalog/api/images/upload/`
  if (ownerUid) {
    url = `${url}${ownerUid}`
  }
  return axios.post(url, formData).then(x => x.data)
}

export { upload }
