Django==1.11.3
django-cors-headers==2.1.0
djangorestframework==3.6.3
djangorestframework-queryfields==1.0.0
olefile==0.44
Pillow==4.2.1
pytz==2017.2
