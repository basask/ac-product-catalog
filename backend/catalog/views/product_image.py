# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@gmail.com)
# @Link    : https://github.com/basask

from django.http import JsonResponse

from catalog.models.image import Image
from catalog.models.product import Product

from catalog.serializers.image import ImageSerializer

def upload_product_image(request, product_uid=None):
    if request.method == 'POST':
        product = Product.objects.filter(
            uid=product_uid
        ).first()
        params = {
            'content': request.FILES['file']
        }
        if product:
            params['product'] = product
        image = Image(**params)
        image.save()
        serializer = ImageSerializer(image)
        return JsonResponse(serializer.data)
    return JsonResponse(status_code=500)
