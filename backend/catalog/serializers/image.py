# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@collabo.com.br)
# @Link    : http://www.collabo.com.br/


from rest_framework import serializers

from catalog.models.image import Image


class ImageSerializer(serializers.ModelSerializer):

    url = serializers.SerializerMethodField()

    class Meta(object):
        model = Image
        fields = ('uid', 'alt', 'url')

    def get_url(self, obj):
        return obj.content.url