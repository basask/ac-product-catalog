# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@collabo.com.br)
# @Link    : http://www.collabo.com.br/


from rest_framework import serializers
from rest_framework.response import Response

from catalog.models.product import Product
from catalog.models.image import Image

from drf_queryfields import QueryFieldsMixin

from catalog.serializers.image import ImageSerializer


class ProductSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    images = ImageSerializer(many=True, required=False)

    class Meta(object):
        model = Product
        fields = ('uid', 'name', 'description', 'parent', 'images',)

    def to_internal_value(self, data):
        value = data.copy()
        images = value.pop('images', [])
        value = super(ProductSerializer, self).to_internal_value(value)
        value['images'] = images
        return value

    def update(self, instance, validated_data):
        validated_data.pop('images', [])
        return super(ProductSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        images = validated_data.pop('images', [])
        product = super(ProductSerializer, self).create(validated_data)
        images_uids = [item.get('uid') for item in images ]
        Image.objects.filter(uid__in=images_uids).update(product=product)
        return product