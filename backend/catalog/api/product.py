# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@collabo.com.br)
# @Link    : http://www.collabo.com.br/


from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from catalog.models.product import Product
# from catalog.models.image import Image

from catalog.serializers.product import ProductSerializer
from catalog.serializers.image import ImageSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    lookup_field = 'uid'

    @detail_route(methods=['get'])
    def images(self, request, uid=None):
        product = self.queryset.get(uid=uid)
        images_serializer = ImageSerializer(product.images, many=True)
        return Response(images_serializer.data)

    @detail_route(methods=['get'])
    def children(self, request, uid=None):
        product = self.queryset.get(uid=uid)
        children = self.queryset.filter(parent=product)
        children_serializer = ProductSerializer(children, many=True)
        return Response(children_serializer.data)

    @detail_route(methods=['get'])
    def parent(self, request, uid=None):
        product = self.queryset.get(uid=uid)
        if product.parent is None:
            return Response(None, status=404)
        parent_serializer = ProductSerializer(product.parent)
        return Response(parent_serializer.data)