# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@collabo.com.br)
# @Link    : http://www.collabo.com.br/

from rest_framework import routers

from catalog.api.product import ProductViewSet

router = routers.SimpleRouter()

router.register(r'products', ProductViewSet)
