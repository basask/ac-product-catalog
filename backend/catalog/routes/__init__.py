# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@collabo.com.br)
# @Link    : http://www.collabo.com.br/


from django.conf.urls import include
from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt


from catalog.routes import api
from catalog import views

urlpatterns = [
    url(r'^api/', include(api.router.urls, namespace='api')),
    # url(r'^api/images/upload/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})', csrf_exempt(views.upload_product_image)),
    url(r'^api/images/upload/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})?', csrf_exempt(views.upload_product_image)),
]



