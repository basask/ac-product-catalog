# Create your models here.
# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@collabo.com.br)
# @Link    : http://www.collabo.com.br/

from uuid import uuid4
from django.db import models


class Product(models.Model):

    uid = models.UUIDField(default=uuid4, editable=False, unique=True, primary_key=True)
    name = models.CharField('Name', max_length=512)
    description = models.CharField('Description', max_length=2048)

    parent = models.ForeignKey('Product', null=True, blank=True)
