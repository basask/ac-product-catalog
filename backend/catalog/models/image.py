# Create your models here.
# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@collabo.com.br)
# @Link    : http://www.collabo.com.br/

from django.db import models
from uuid import uuid4
from os.path import splitext

def upload_image_path(image, filename):
    _, ext = splitext(filename)
    return 'product/{}{}'.format(image.uid, ext)


class Image(models.Model):
    uid = models.UUIDField(default=uuid4, editable=False, unique=True, primary_key=True)
    product = models.ForeignKey('Product', null=True, related_name='images')
    alt = models.CharField('Name', max_length=512, blank=True)
    content = models.ImageField(upload_to=upload_image_path)
