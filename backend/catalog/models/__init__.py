# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@collabo.com.br)
# @Link    : http://www.collabo.com.br/


from catalog.models.image import Image
from catalog.models.product import Product
