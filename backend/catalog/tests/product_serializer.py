from django.test import TestCase

from catalog.serializers.product import ProductSerializer


class TestProductSerializer(TestCase):
    def test_basic_validations_in_serializer_intantiation(self):
        serializer = ProductSerializer(data={})
        self.assertFalse(serializer.is_valid())

    def test_error_messages_on_missing_data_for_serializer(self):

        serializer = ProductSerializer(data={
            'name': 'Some name'
        })
        self.assertFalse(serializer.is_valid())

        errors = serializer.errors
        self.assertIsNone(errors.get('name'))
        self.assertIsNotNone(errors.get('description'))

    def test_successfull_serialization_and_validation(self):
        serializer = ProductSerializer(data={
            'name': 'Some name',
            'description': 'Some description'
        })
        self.assertTrue(serializer.is_valid())
