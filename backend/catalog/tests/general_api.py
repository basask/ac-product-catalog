import json

from django.test import TestCase
from rest_framework.test import APIClient
from catalog.models.image import Image
from django.core.files.uploadedfile import SimpleUploadedFile


class TestGeneralApi(TestCase):

    def __init__(self, *args, **kwargs):
        super(TestGeneralApi, self).__init__(*args, **kwargs)
        self.client = APIClient()

    def fake_file(self, filename):
        return SimpleUploadedFile(filename, b'No File content here. Just a Mock')

    def setUp(self):
        self.imageA = Image.objects.create(
            alt="image-A", content=self.fake_file('imageA.jpg'))
        self.imageB = Image.objects.create(
            alt="image-B", content=self.fake_file('imageB.jpg'))

    def test_sucessfully_create_new_product_and_ge_its_images(self):
        payload = {
            'name': 'Product 1',
            'description': 'Product 1 description',
            'images': [
                {
                    'uid': str(self.imageA.uid),
                    'alt': ''
                }
            ]
        }
        payload = json.dumps(payload)

        response = self.client.post('/catalog/api/products/', payload, content_type='application/json')
        # Accepting 200 and 201, high level and detailed, http status for now
        self.assertTrue(response.status_code in [200, 201])
        product = dict(response.data)

        uid = product.get('uid')

        response = self.client.get('/catalog/api/products/' + uid + '/?fields=images', format='json')

        product = dict(response.data)
        self.assertTrue('images' in product)
        self.assertFalse('description' in product)

        images = product.get('images')

        self.assertEqual(len(images), 1)

        image = images[0]
        self.assertTrue('url' in image)
        self.assertTrue('alt' in image)
        self.assertTrue('uid' in image)


    def test_image_subresource_in_product(self):
        payload = {
            'name': 'Product 2',
            'description': 'Product 2 description',
            'images': [
                {
                    'uid': str(self.imageB.uid),
                    'alt': ''
                }
            ]
        }
        payload = json.dumps(payload)

        response = self.client.post('/catalog/api/products/', payload, content_type='application/json')

        product = dict(response.data)
        product_uid = product.get('uid')

        response = self.client.get('/catalog/api/products/' + product_uid + '/images/')

        self.assertEqual(len(response.data), 1)
