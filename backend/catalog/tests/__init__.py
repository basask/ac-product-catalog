# -*- coding: utf-8 -*-
# @Author  : Rafael Fernandes (basask@gmail.com)
# @Link    : https://github.com/basask


from catalog.tests.product_serializer import TestProductSerializer
from catalog.tests.product_api import TestProductApi
from catalog.tests.general_api import TestGeneralApi
