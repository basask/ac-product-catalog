from django.test import TestCase

from rest_framework.test import APIClient


class TestProductApi(TestCase):

    def __init__(self, *args, **kwargs):
        super(TestProductApi, self).__init__(*args, **kwargs)
        self.client = APIClient()

    def test_product_listing_with_no_products_in_database(self):
        response = self.client.get('/catalog/api/products/', format='json')
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), 0)

    def test_sucessfully_create_new_product(self):

        payload = {
            'name': 'Product 1',
            'description': 'Product 1 description'
        }

        response = self.client.post('/catalog/api/products/', payload, format='json')
        # Accepting 200 and 201, high level and detailed, http status for now
        self.assertTrue(response.status_code in [200, 201])
        product = dict(response.data)

        self.assertTrue('uid' in product)
        self.assertTrue('name' in product)
        self.assertTrue('description' in product)
        self.assertTrue('parent' in product)


    def test_missing_data_on_product_creation(self):

        payload = {
            'description': 'Product 2 description'
        }

        response = self.client.post('/catalog/api/products/', payload, format='json')
        # Considering booth high level error code 400 and more detailed code 422
        self.assertTrue(response.status_code in [400, 422])
        errors = dict(response.data)
        self.assertTrue('name' in errors)

    def test_product_listing_with_products_in_database(self):

        payload = {
            'name': 'Product 3',
            'description': 'Product 3 description'
        }
        self.client.post('/catalog/api/products/', payload, format='json')

        response = self.client.get('/catalog/api/products/', format='json')

        self.assertEquals(response.status_code, 200)

        self.assertEquals(len(response.data), 1)

        products = list(response.data)

        for product in products:
            self.assertTrue('uid' in product)
            self.assertTrue('name' in product)
            self.assertTrue('description' in product)
            self.assertTrue('parent' in product)


    def test_product_creation_with_parent_in_payload(self):
        payload = {
            'name': 'Product 4',
            'description': 'Product 4 description'
        }
        response = self.client.post('/catalog/api/products/', payload, format='json')
        parent_product = dict(response.data)
        parent_uid = parent_product.get('uid')

        payload = {
            'name': 'Product 4',
            'description': 'Product 4 description',
            'parent': parent_uid
        }
        response = self.client.post('/catalog/api/products/', payload, format='json')
        product = dict(response.data)

        self.assertTrue('uid' in product)
        self.assertTrue('name' in product)
        self.assertTrue('description' in product)

        self.assertEqual(str(product.get('parent')), parent_uid)

    def test_patching_product_parent_after_its_creation(self):
        payload = {
            'name': 'Product 5',
            'description': 'Product 5 description'
        }
        response = self.client.post('/catalog/api/products/', payload, format='json')
        parent_product = dict(response.data)
        parent_uid = parent_product.get('uid')

        payload = {
            'name': 'Product 6',
            'description': 'Product 6 description'
        }
        response = self.client.post('/catalog/api/products/', payload, format='json')

        payload = '{"parent": "%s"}' % parent_uid

        uid = response.data.get('uid')
        product_url = '/catalog/api/products/' + uid + '/'

        # Bug on djangorestframework: Patch method does not accept format=json like other methods (get, post, etc)
        response = self.client.patch(product_url, payload, content_type='application/json')

        response = self.client.get(product_url, format='json')
        product = dict(response.data)

        self.assertTrue('uid' in product)
        self.assertTrue('name' in product)
        self.assertTrue('description' in product)

        self.assertEqual(str(product.get('parent')), parent_uid)

    def test_getting_children_products(self):
        payload = {
            'name': 'Product 7',
            'description': 'Product 7 description'
        }
        response = self.client.post('/catalog/api/products/', payload, format='json')
        parent_product = dict(response.data)
        parent_uid = parent_product.get('uid')

        parent_url = '/catalog/api/products/' + parent_uid + '/'

        response = self.client.get(parent_url + 'children/')
        self.assertEquals(response.status_code, 200)
        children = dict(response.data)
        self.assertEqual(len(children), 0)

        payload = {
            'name': 'Product 8',
            'description': 'Product 8 description',
            'parent': parent_uid
        }

        self.client.post('/catalog/api/products/', payload, format='json')

        response = self.client.get(parent_url + 'children/')
        self.assertEquals(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

        child = dict(response.data[0])

        self.assertTrue('uid' in child)
        self.assertTrue('name' in child)
        self.assertTrue('description' in child)
        self.assertEqual(str(child.get('parent')), parent_uid)

    def test_create_and_get_product_hierarchy_through_parent_mechanism(self):
        payload = {
            'name': 'Product 9',
            'description': 'Product 9 description'
        }
        response = self.client.post('/catalog/api/products/', payload, format='json')
        parent_product = dict(response.data)
        parent_uid = parent_product.get('uid')

        response = self.client.get('/catalog/api/products/' + parent_uid + '/parent/')

        self.assertTrue(response.status_code in [400, 404])

        payload = {
            'name': 'Child Product 9.1',
            'description': 'Product 9.1 description',
            'parent': parent_uid
        }
        response = self.client.post('/catalog/api/products/', payload, format='json')

        child = dict(response.data)
        child_uid = child.get('uid')

        response = self.client.get('/catalog/api/products/' + child_uid + '/parent/')
        parent = dict(response.data)
        self.assertEqual(parent_uid, parent.get('uid'))